Apertis health checks
=====================

A collection of tools scraping information from various data sources to ensure
that everything is sane on the Apertis infrastructure.
